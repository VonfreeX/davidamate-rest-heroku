var express = require('express')
var db =require('./db.js')
var app = express()
var bodyParser=require("body-parser");
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({extended:true}));
app.get('/', function (req, res){
	res.send('Hello World!')
});
app.get('/all', function (req, res){
	db.query("SELECT * FROM app",function(err,result){
	if(!err){
		res.json(result.rows)
	}
		else {
		res.send("Error a la db");
		console.log(err);
		}
	});
});
app.get('/:app_code', function (req, res){
	db.query("SELECT * FROM app WHERE app_code=$1",[req.params.app_code],function(err,result){
	if(!err){
		res.json(result.rows)
	}
		else {
		res.send("Error a la db");
		console.log(err);
		}
	});
});

app.post('/:app_code', function (req, res){
	db.query("INSERT INTO records(app_code,player,score) VALUES($1, $2,$3)", [req.params.app_code, req.body.player,req.body.score],function(err,result){
	if(!err){
		res.json(result.rows)
	}
		else {
		res.send("Error a la db");
		console.log(err);
		}
	});
});
app.listen(process.env.PORT||3000, function () {
	console.log('Example app listening on port 3000!'
)})
